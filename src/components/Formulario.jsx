import React, { Fragment, useState } from 'react'
import uuid from 'uuid/v4'
import PropTypes from 'prop-types'

const Formulario = ({crearCita}) => {

    //creando state de citas
    const [cita, actualizarCita] = useState({
        mascota:'',
        dueño:'',
        fecha:'',
        hora:'',
        sintomas:'',
    })

    //creando otro estado para un posible error en la validacion del formulario

    const [error, actualizarError] = useState(false)

    //funcion que se ejecuta cuando el usuario escribe en el formulario
    const handleChange = e => {
        actualizarCita({
            ...cita,
            [e.target.name]: e.target.value
        })
    }

    //extraer los valore
    const { mascota, dueño, fecha, hora, sintomas } = cita

    //cuando el usuario envia el formulario

    const submitCita = e => {
        e.preventDefault()
        //validar
        if (mascota.trim() === '' || dueño.trim() === '' || fecha.trim() === '' || hora.trim() === '' || sintomas.trim() === '') {
            actualizarError(true)
            return
        }

        //eliminar mensaje previo de error
        actualizarError(false)

        //asignar ID
        cita.id = uuid()
        console.log(cita)

        //crear cita
        crearCita(cita)

        
        //reiniciar el formulario
        actualizarCita({
            mascota:'',
            dueño:'',
            fecha:'',
            hora:'',
            sintomas:'',
        })
    }

    return ( 
        <Fragment>
            <h2>Crear Cita</h2>

            { error ? <p className="alerta-error">Todos los campos son obligatorios</p> : null }

            <form
             onSubmit={submitCita}
            >
                <label>Nombre Mascota</label>
                <input 
                    type="text"
                    name="mascota"
                    className="u-full-width"
                    placeholder="Nombre Mascota"
                    onChange={handleChange}
                    value={mascota}
                />
                <label>Nombre Dueño</label>
                <input 
                    type="text"
                    name="dueño"
                    className="u-full-width"
                    placeholder="Nombre del Dueño"
                    onChange={handleChange}
                    value={dueño}
                />
                <label>Fecha</label>
                <input 
                    type="date"
                    name="fecha"
                    className="u-full-width"
                    onChange={handleChange}
                    value={fecha}
                />
                <label>Hora</label>
                <input 
                    type="time"
                    name="hora"
                    className="u-full-width"
                    onChange={handleChange}
                    value={hora}
                />
                <label>Síntomas</label>
                <textarea 
                    name="sintomas" 
                    className="u-full-width"
                    onChange={handleChange}
                    value={sintomas}
                ></textarea>
                <button
                    type="submit"
                    className="u-full-width button-primary"
                >AGREGAR CITA</button>
            </form>
        </Fragment>   
);
}

Formulario.propTypes = {
    crearCita: PropTypes.func.isRequired
}
 
export default Formulario;