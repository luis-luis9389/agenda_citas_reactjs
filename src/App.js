import React, { Fragment, useState, useEffect } from 'react';
import Formulario from './components/Formulario'
import Citas from './components/Citas'


function App() {

  //citas en local storage
  let citasIniciales = JSON.parse(localStorage.getItem('citas'))
  if (!citasIniciales) {
    citasIniciales = []
  }

  const [citas, guardarCitas] = useState(citasIniciales)

  useEffect(() => {
    let citasIniciales = JSON.parse(localStorage.getItem('citas'))
    if (citasIniciales) {
      localStorage.setItem('citas', JSON.stringify(citas))
    } else {
      localStorage.setItem('citas', JSON.stringify([]))
    }
  }, [citas])

  //funcion que toma las citas actuales y agrega nuevas
  const crearCita = cita => {
    guardarCitas([
      ...citas,
      cita
    ])
  }

  //funcion que elimina cita por id
  const eliminarCitas = id => {
    const nuevasCitas = citas.filter(cita => cita.id !== id)
    guardarCitas(nuevasCitas)
  }

  //mensaje condicional
  const titulo = citas.length === 0 ? 'NO HAY CITAS' : 'ADMINISTRA TUS CITAS'

  return (
    <Fragment>
      <h1>Administrador de Pacientes</h1>
      <div className="container">
        <div className="row">
          <div className="one-half column">
            <Formulario
              crearCita={crearCita}
            />
          </div>
          <div className="one-half column">
            <h2>{titulo}</h2>
            {citas.map(cita => (
              <Citas
                key={cita.id}
                cita={cita}
                eliminarCitas={eliminarCitas}
              />
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
